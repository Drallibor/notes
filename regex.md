# Expressions régulières : résumé

## Délimiteurs

Les plus souvent utilisés : `/` `#` et `~`
Exemples : 
- `/foo bar/`
- `#foo bar#`
- `~foo bar~`

## Classes de caractères
| Code | Description | Exemple |
|------|-------------|---------|
|`[...]`|Ouverture et fermeture d'une classe de caractères|`m[aeo]t` équivaut à `mot\|met\|mat`|
|`[a-z]`|Intervale des lettres minuscules de a à z |`^m[a-z]t$` peut retourner `mat`, `met`, `mit`, `mot` ou `mut` si on cherche dans un dictionnaire français |
|`(?<name>selection)`|Groupe nommé||

## Métacaractères
| Hors crochets | Dans crochets | Description |
|------|------|-------------|
|`\`|`\`|Caractère d'échappement, avec de multiples usages|
|`^`||Le début de la chaîne sujet (ou de ligne, en mode multilignes)|
||`[^...]`|Négation de caractères|
|`$`||La fin de la chaîne sujet ou avant la fin d'une nouvelle ligne (ou fin de ligne, en mode multilignes)|
|`.`||Remplace n'importe quel caractère, hormis le caractère de nouvelle ligne (par défaut)|
|`\|`||Caractère de début d'alternative : opérateur OU|
|`(`||Caractère de début de sous-masque|
|`)`||Caractère de fin de sous-masque|
|`?`||Étend le sens de (; quantificateur de 0 ou 1; quantificateur de minimisation (Voir les répétitions)|
|`*`||Quantificateur de 0 ou plus|
|`+`||Quantificateur de 1 ou plus|
|`{`||Caractère de début de quantificateur minimum/maximum|
|`}`||Caractère de fin de quantificateur minimum/maximum|

## Quantificateurs
| Code | Description | Exemple |
|------|-------------|---------|
|`{min,max}`|Le nombre de répétitions varie entre la valeur minimale et la valeur maximale incluses||
|`{min,}`|Le nombre de répétitions varie entre la valeur minimale incluse et l'infini||
|`{,max}`|Le  nombre de répétitions varie entre 0 et la valeur maximale incluse||
|`{nombre}`|Le nombre de répétitions correspond au nombre marqué entre les accolades||
|`*`|0 ou plusieurs répétitions|Équivaut à `{0,}`|
|`+`|1 ou plusieurs répétitions|Équivaut à `{1,}`|
|`?`|0 ou 1 occurence|Équivaut à `{,1}`|


## Séquences échappées
| Code | Description | Exemple |
|------|------|-------------|
|`\0`|Motif de recherche global||
|`\x`|Où `x` est un nombre entre 1 et 9 : désigne le sous-ensemble par ordre de parenthèse ouvrante.||
|`\w`|Tout caractère alphanumérique|Équivalent de `[a-zA-Z0-9_]`|
|`\d`|Caractère décimal ("d" pour "digit")|Équivalent de `[0-9]`|
|`\n`|Nouvelle ligne||
|`\r`|Retour chariot||
|`\s`|Espacement (space)|Équivaut à `[ \t\r\n\f]`|
|`\t`|Tabulation||

-------------------------

# Sources
- [Référence PCRE](https://www.pcre.org/)
- [Référence PCRE dans PHP](https://www.php.net/manual/fr/intro.pcre.php)
- [Référence Markdown](https://www.markdownguide.org)
- [Testeur Regex101](https://regex101.com/)
- [Tuto initiation](https://www.lucaswillems.com/fr/articles/25/tutoriel-pour-maitriser-les-expressions-regulieres)
- [Tuto Zeste de savoir](https://zestedesavoir.com/tutoriels/3651/les-expressions-regulieres-1/)

-------------------------

# Exemples

## Chaînes à tester
/usr/share/dict/french

"Bonjour et au revoir ! Je m'appelle John Doe, j'ai 27 ans, j'habite en France et travaille depuis que j'ai 20 ans. Ma passion : écrire des mots, mits, mets, mats, mat... Pour me contacter, vous pouvez envoyer un email à contact@johndoe.fr ou contact@johndoe.com ou bien m'appeler au 06 07 08 09 10. Vous pouvez aussi aller voir mon blog à l'adresse johndoe-blog.fr. Bonjour et au revoir"

## Regex fréquentes
- Numéro de téléphone français : `#(0|\+33)[1-9]( *[0-9]{2}){4}#`
- URL : `#https?://[a-zA-Z0-9-\.]+\.[a-zA-Z]{2,4}(/\S*)?#`
- Date JJ/MM/AAAA - utilisation des groupes nommés : `(?<day>\d+)\/(?<month>\d+)\/(?<year>\d+)`
